'use strict';

const gulp = require('gulp')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')
const stylus = require('gulp-stylus')
const buffer = require('vinyl-buffer')
const source = require('vinyl-source-stream')
const browserify = require('browserify')
const babelify = require('babelify')
const sourcemaps = require('gulp-sourcemaps')
const watchify = require('watchify')


const bundle = () => {
    return browserify({
            entries: './static/dev/js/index.js',
            transform: [babelify],
            debug: true
        })
        .transform(babelify, { presets: ["es2015", "es2017", "stage-0"] })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./static/js'))
}

const scriptsDev = () => {
    return browserify({
            entries: './static/dev/js/index.js',
            transform: ['babelify']
        })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('./static/js'))
}

gulp.task('bundle', function() {
    scriptsDev()
})

gulp.task('bundleProduccion', function() {
    bundle()
})

gulp.task('default', ['bundle'])
gulp.task('css', ['estilos'])